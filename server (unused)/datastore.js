/**
 * A simple Datastore that store an array of events per user. Will be replaced by Mongo later
 */

const { sendEmail } = require('./mailer');
const { SERVER_URL } = require('./client/src/config.json')[process.env.NODE_ENV];

// a mapping of user->[{type,payload}]
let eventTable = {};

const addEvent = (user, event) => {
    if (!eventTable[user]) eventTable[user] = [];
    eventTable[user].push({ ...event, timestamp: Date.now() });
    console.log(`addEvent: ${JSON.stringify(eventTable, null, 2)}`);
    sendEmail(
        user,
        `${event.name} verified email`,
        `<p>Dear ${event.therapist},</p>` +
            `<p>${event.name} has verified their email.<br/>` +
            `You can now send them invoices by visiting <a href='${SERVER_URL}'>Spot App</a>.</p>` +
            `<p>Regards,<br/>SPOT team</p>`
    );
    return '<h3>Thank you for verifying your email. You will receive all invoices on this email address</h3>';
};

const getAllEvents = (user) => {
    console.log(`getAllEvents: found ${eventTable[user] ? eventTable[user].length : 0} events for ${user}`);
    return eventTable[user] ? eventTable[user] : [];
};

const deleteEvent = (user, _key) => {
    if (!eventTable[user]) return;
    eventTable[user] = eventTable[user].filter(({ key }) => key !== _key);
    console.log(`deleteEvent: ${JSON.stringify(eventTable, null, 2)}`);
};

module.exports = { addEvent, getAllEvents, deleteEvent };
