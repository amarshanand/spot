const cors = require('cors');
const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 3001;

const { addEvent, getAllEvents, deleteEvent } = require('./datastore');

app.use(cors());

// routes
app.get('/addEvent', (req, res) => {
    res.send(addEvent(req.query.user, JSON.parse(req.query.event)));
});

app.get('/events', (req, res) => {
    res.send(getAllEvents(req.query.user));
});

app.get('/deleteEvent', (req, res) => {
    res.send(deleteEvent(req.query.user, req.query.key));
});

//production mode
if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.join(__dirname, 'client/build')));
    app.get('*', (req, res) => {
        res.sendfile(path.join((__dirname = 'client/build/index.html')));
    });
}

app.listen(port, () => console.log(`Server listening on port ${port}!`));
