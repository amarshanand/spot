[![SPOT - Complete walthrough](./video_image.png)](https://www.youtube.com/watch?v=sV_gI2pPX7Y)

# SPOT

SPOT (Speech Pathology and Occupational Therapy) is a webapp that allows a Therapist to do Admin work. In the first phase, we are dealing with Invoicing and Payments. We are using `React (with hooks)` + `material-ui`

# Live deployment

https://spotinvoices.com

# Architecture

SPOT exploits common Google services to create a standard form-based app experience. Consider the following sequence:

### Therapist signin and auth

Google signin service. This neccessiates that the Therapist must have a Gmail account.

### Saving Therapist Data

Google Drive is the place where all data is saved. Upon signup, a new folder called `SPOT_DATA` gets created in the therapist's Google Drive's root dir. A spreadsheet called `settings` gets created where all info regarding the therpaist is saved

### Adding a new patient

Upon creating a patient, a new folder for that patient gets created. This folder gets shared (read-only) with the patient. A spreadsheet called `records` gets created, which lists all sessions the therapist had had with the patient.

### Generating and sending Invoices

An Invoice template has been created with public read-only access as a Google Doc. Upon creating a new session, the therpist can chhose to send an invoice. This process involves, copying the Invoice Template to the patients's drive (ie, the folder that is being shared with the patient), and populating it with the data for the session. Once ready, this Google Doc is shared (readonly) with the patient.

_Note that the patient is not required to have a Gmail account._

# Techincal Architecture

Consider SPOT as a pure Frontend app, with absolutely no server whatsoever. The only purpose we have a server at present is to server the SPOT App.

The problems with this pure-client architecture is that if the patient wants to communicate something to the therapist, they cant (since everything is readonly to them). To mitigate this, I have created a simple event-queue type server (curently used for nothing).

# Running SPOT

## Creating DNS entry for running the Dapp locally

I had some issues with whitelisting callbacks to `http://localhost:3000` for logging-in through Gmail. Hence, I had to create a local DNS entry that appears as a legetimate url to Google. In Mac, the steps are:

```
# open the /etc/hosts file
% sudo nano /etc/hosts

# and add the following to it
127.0.0.1       www.mylocalmachine.com

# and flush the DNS cache
% sudo killall -HUP mDNSResponder;say DNS cache has been flushed
```

Consequently, you'd notice that `npm run start` attempts to start `https://www.mylocalmachine.com:3000`

## running locally

```
% cd client/
% npm install
% npm  run start
```

## for production

Upon making a commit to `master`, the CI/CD kicks in. It does a `npm run build`, and deploys the server to heroku.
