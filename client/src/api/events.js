/**
 * A set of APIs that interact with Messaging App
 */

const { SERVER_URL } = require('../config.json')[process.env.REACT_APP_ENV];

const getEvents = (user) => fetch(`${SERVER_URL}/events?user=${user}`).then((response) => response.json());

const deleteEvent = (user, key) => fetch(`${SERVER_URL}/deleteEvent?user=${user}&key=${key}`);

export { getEvents, deleteEvent };
