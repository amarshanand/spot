/**
 * A set of APIs that interact with Google Drive and Google Sheets
 */
let drive,
  spreadsheets,
  documents,
  gmail,
  spotDataFolderId,
  settingsSpreadsheetId,
  templatesFolderId,
  invoiceTemplateId,
  feedbackTemplateId,
  patientFolders = {}, // mappings of patient_folder_name -> id
  idToName = {},
  settings;

const { starterDocsFolder } = require("../config.json");

const setSettingsSpreadsheetId = (_settingsSpreadsheetId) =>
  (settingsSpreadsheetId = _settingsSpreadsheetId);

/**
 * create a file or a folder of the given type, name and parents
 * @param mimeType FOLDER or file
 * @param name can be in format <parentFolderId>/name. <parentFolderId> defaults to SPOT_DATA
 * @param isRoot if true, create in Google Drive's root folder, else create in SPOT_DATA
 */
const SHEET = "application/vnd.google-apps.spreadsheet";
const FOLDER = "application/vnd.google-apps.folder";
const createAsset = async (
  mimeType,
  name,
  firstSheetTitle = "main",
  isRoot = false
) => {
  try {
    let [parents, title] = name.includes("/")
      ? name.split("/")
      : [spotDataFolderId, name];
    parents = isRoot ? [] : [parents];
    const {
      result: { id },
    } = await drive.files.create({
      resource: { name: title, mimeType, parents },
    });
    console.log(
      `created ${mimeType === SHEET ? "spreadsheet" : "folder"} ${name}:${id}`
    );
    idToName[id] = name;
    if (mimeType === SHEET) await renameSheet(id, 0, firstSheetTitle); // rename the first sheet
    return id;
  } catch (e) {
    console.error(`Failed to create ${name}`);
  }
};

/**
 * rename the given asset
 * @param assetId the id of the asset that requires renaming
 * @param name should be absoluet (ie, no paths with / )
 */
const renameAsset = async (assetId, name) => {
  try {
    // see if this assetId had a name against it already. if so, and if it is the same as or assetId, do nothing
    const entry = Object.entries(patientFolders).find(
      ([, { folderId }]) => folderId === assetId
    );
    if (entry[0] === name) return;
    await drive.files.update({ fileId: assetId, name });
    console.log(`renamed ${assetId} to ${name}`);
    patientFolders[name] = entry[1];
    delete patientFolders[entry[0]];
  } catch (e) {
    console.error(`Failed to rename to ${name}`);
  }
};

/**
 * find a file or a folder of the given type, name and parents
 * @param name can be in format <parentFolderId>/name. <parentFolderId> defaults to SPOT_DATA
 * @param isRoot if true, create in Google Drive's root folder, else create in SPOT_DATA
 */
const findAsset = async (name, isRoot) => {
  try {
    let [parents, title] = name.includes("/")
      ? name.split("/")
      : [spotDataFolderId, name];
    parents = isRoot ? null : parents;
    const q = `name = '${title}' and trashed = false ${
      parents ? "and '" + parents + "' in parents" : ""
    }`;
    const {
      result: { files },
    } = await drive.files.list({ q });
    console.log(`found ${name}:${files[0].id}`);
    idToName[files[0].id] = name;
    return files[0].id;
  } catch (e) {
    console.log(`Could not find ${name}`);
  }
};

/**
 * Copy the file with the given assetId and paste to the passed name.
 * Public sheets from other accounts can be copied too
 * @param {*} assetId
 */
const copyAsset = async (assetId, name) => {
  try {
    let [parents, title] = name.includes("/")
      ? name.split("/")
      : [spotDataFolderId, name];
    parents = parents ? [parents] : [];
    const {
      result: { id, webViewLink },
    } = await drive.files.copy({
      fileId: assetId,
      name: title,
      parents,
      fields: "id, webViewLink",
    });
    console.log(`copied file ${assetId} to ${name}:${id}`);
    return { id, webViewLink };
  } catch (e) {
    console.log(`Failed to copy ${assetId} : ${JSON.stringify(e)}`);
  }
};

/**
 * Copy a folder across, by listing all files and copying them one by one.
 * @param {*} folderId
 * @param {*} name
 */
const copyFolder = async (folderId, folderName) => {
  const files = await listFolder(folderId);
  const newFolderId = await createAsset(FOLDER, folderName);
  for (const { id, name, mimeType } of files)
    mimeType === FOLDER
      ? await copyFolder(id, `${newFolderId}/${name}`)
      : await copyAsset(id, `${newFolderId}/${name}`);

  console.log(`copied folder ${folderId} to ${newFolderId}:${folderName}`);
  return newFolderId;
};

/**
 * share the given folder (read only) with the given email
 */
const shareAsset = async (fileId, emailAddress, emailMessage) => {
  const permission = { type: "user", role: "reader", emailAddress };
  if (emailMessage)
    await drive.permissions.create({
      resource: permission,
      fileId,
      sendNotificationEmail: true,
      emailMessage,
    });
  else
    await drive.permissions.create({
      resource: permission,
      fileId,
      sendNotificationEmail: false,
    });
  console.log(`shared asset ${fileId} with ${emailAddress}`);
};

/**
 * get a sharable link for the given file or folder
 */
const getSharableLink = async (fileId) => {
  const {
    result: { webViewLink },
  } = await drive.files.get({
    fileId,
    fields: "webViewLink",
  });
  console.log(`obtained webViewLink as ${webViewLink} for ${fileId}`);
  return webViewLink;
};

/**
 * download the given asset as PDF
 */
const downloadAsPDF = async (fileId) => {
  const { body } = await drive.files.export({
    fileId,
    mimeType: "application/pdf",
  });
  console.log(`downloaded asset ${fileId} as PDF`);
  return body;
};

/**
 * list a folder
 */
const listFolder = async (assetId, mimeType) => {
  try {
    console.log(`listing ${assetId} for mimeType ${mimeType}`);
    let q = `'${assetId}' in parents and trashed = false`;
    if (mimeType) q += ` and mimeType = '${mimeType}'`;
    const {
      result: { files },
    } = await drive.files.list({
      includeRemoved: false,
      q,
      fields: "files(id, name, mimeType)",
    });
    files.forEach(({ id, name }) => (idToName[id] = name));
    return files;
  } catch (e) {
    console.log(`Failed to list ${assetId}`);
  }
};

/**
 * obtain the set of values from the given sheetId. this method returns a complete set of values from all sheets
 */
const getSpreadsheet = async (spreadsheetId) => {
  console.log(
    `reading spreadsheet ${idToName[spreadsheetId]}:${spreadsheetId}`
  );
  // first obtain a list of all sheets in the spreadsheets
  let values = {};
  const {
    result: { sheets },
  } = await spreadsheets.get({ spreadsheetId });
  // and now, obtain each sheet's data
  for (const {
    properties: { title },
  } of sheets) {
    const _values = await getSheet(spreadsheetId, title);
    if (title === "main") values = { ...values, ..._values[0] };
    else values[title] = _values;
  }
  return values;
};

/**
 * obtain the set of values from the given sheetId in the given range (defaults to main sheet)
 * we eventually obtain an array if nvps, much like [row1, row2, row3], and row = {heading1: value1, heading2: value2}
 */
const getSheet = async (spreadsheetId, range = "main") => {
  console.log(
    `reading sheet ${range} in ${idToName[spreadsheetId]}:${spreadsheetId}`
  );
  const {
    result: { values },
  } = await spreadsheets.values.get({ spreadsheetId, range });
  if (!values) return {};
  // the spreadsheet returns an [] of rows, the first row being the heading. we produce an array of {nvp} from it
  const nvps = [];
  for (let i = 1; i < values.length; i++) {
    let nvp = {};
    values[0].forEach(
      (heading, index) =>
        (nvp[heading] = values[i][index] ? values[i][index] : "")
    );
    nvps.push(nvp);
  }
  return nvps;
};

/**
 * change the title of a sheet in the given spreadsheet
 */
const renameSheet = async (spreadsheetId, index, title) => {
  console.log(
    `renaming sheet ${idToName[spreadsheetId]}:${spreadsheetId}~${index} to ${title}`
  );
  const {
    result: { sheets },
  } = await spreadsheets.get({ spreadsheetId });
  const { sheetId } = sheets[index].properties;
  const resource = {
    requests: [
      {
        updateSheetProperties: {
          properties: { sheetId, title },
          fields: "title",
        },
      },
    ],
  };
  await spreadsheets.batchUpdate({ spreadsheetId, resource });
};

/**
 * save the given data in the given spreadsheet. we interperate `values` as a tree.
 * consider `values = {name:'', age:'', sessions:[{date:'', title:''}]}`
 * this would save `name` and `age` in the `main` sheet,
 * and would save a series of sessions (ie, `date`, `title`) in a sheet called `sessions`
 * `columns` is only for second-level-data (which gets saved as a table in a separate sheet).
 * it denotes the headings of each of the columns (say, for sessions, it will be ['id', 'timestamp, ...])
 */
const saveSpreadsheet = async (
  spreadsheetId,
  values,
  fileName,
  folderName,
  columns
) => {
  //console.log(`saveSpreadsheet will save`);
  //console.dir(values);
  console.log(
    `saving ${folderName ? folderName + "/" : ""}${fileName}${
      spreadsheetId ? ":" + spreadsheetId : ""
    }`
  );
  // ensure that the given folderName exist
  if (folderName && !patientFolders[folderName])
    patientFolders[folderName] = {
      folderId: await createAsset(FOLDER, folderName),
    };
  // and ensure that the given spreadsheet exist
  if (!spreadsheetId)
    spreadsheetId = await createAsset(
      SHEET,
      `${
        folderName ? patientFolders[folderName]["folderId"] + "/" : ""
      }${fileName}`
    );
  // first we flatten our tree, ie, separate out the first level and second level data
  let sheetToValuesMap = {};
  values = values.filter((value) => {
    if (!Array.isArray(value[1])) return true;
    if (!columns) return false;
    // second level data would contain an Array of Objects . we need to collapse them before sending to sheets
    const collapsedArray = [];
    for (let i = 0; i < columns.length; i++) {
      const col = columns[i];
      collapsedArray[i] = [col];
      for (let row of value[1]) collapsedArray[i].push(row[col]);
    }
    sheetToValuesMap[value[0]] = collapsedArray;
    return false;
  });
  sheetToValuesMap["main"] = values;
  // now the flatten structure contains an array of [sheet,values[]]. first we ensure that all sheets are present
  await ensureAllSheetsArePresent(spreadsheetId, Object.keys(sheetToValuesMap));
  // next, we update all sheets with the new values
  for (let entry of Object.entries(sheetToValuesMap)) {
    await spreadsheets.values.clear({ spreadsheetId, range: `${entry[0]}` });
    await spreadsheets.values.batchUpdate(
      { spreadsheetId },
      {
        valueInputOption: "RAW",
        data: [
          {
            range: `${entry[0]}!A1`,
            majorDimension: "COLUMNS",
            values: entry[1],
          },
        ],
      }
    );
  }
  // if we are updating settings, update the variable in this file too
  if (fileName === "settings") {
    settings = values.reduce(function (prev, curr) {
      prev[curr[0]] = curr[1];
      return prev;
    }, {});
    return { id: spreadsheetId, folderId: spotDataFolderId };
  }
  // if its a `records` sheet, update the folder entry
  if (fileName === "records")
    patientFolders[folderName].recordsSpreadsheetId = spreadsheetId;
  return {
    id: spreadsheetId,
    folderId: patientFolders[folderName]["folderId"],
  };
};

/**
 * ensure that all the sheets in allSheets are present in the given spreadsheet. If not, create them.
 */
const ensureAllSheetsArePresent = async (spreadsheetId, allSheets) => {
  const {
    result: { sheets },
  } = await spreadsheets.get({ spreadsheetId });
  const existingSheets = sheets.map(({ properties: { title } }) => title);
  const requests = [];
  allSheets.forEach(
    (title) =>
      !existingSheets.includes(title) &&
      requests.push({ addSheet: { properties: { title } } })
  );
  if (requests.length > 0)
    await spreadsheets.batchUpdate({ spreadsheetId, requests });
};

/**
 * Send an email without Attachment. All communications are CCed to altEMails
 */
const sendEmailWithoutAttachment = (recipient, subject, message, done) => {
  var body =
    `From: me\r\n` +
    `To: ${recipient}\r\n` +
    `CC: ${settings.altEmails}\r\n` +
    `Subject: ${subject}\r\n` +
    `Content-Type: text/html; charset=UTF-8\r\n` +
    `\r\n${message}\r\n`;
  const raw = btoa(unescape(encodeURIComponent(body)))
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=+$/, "");
  const request = gmail.users.messages.send({
    userId: "me",
    resource: { raw },
  });
  request.execute(() => {
    console.log(`sent email to ${recipient} with subject : ${subject}`);
    done && done();
  });
};

/**
 * Send an email. All communications are CCed to altEMails
 */
const sendEmail = (recipient, subject, message, pdfData, pdfFileName, done) => {
  if (!pdfData || !pdfFileName)
    return sendEmailWithoutAttachment(recipient, subject, message, done);
  const body = [
    `Content-Type: multipart/mixed; boundary="message_boundary"\r\n`,
    `MIME-Version: 1.0\r\n`,
    `From: me\r\n`,
    `To: ${recipient}\r\n`,
    `CC: ${settings.altEmails}\r\n`,
    `Subject: ${subject}\r\n\r\n`,
    `--message_boundary\r\n`,
    `Content-Type: text/html; charset="UTF-8"\r\n`,
    `MIME-Version: 1.0\r\n`,
    `Content-Transfer-Encoding: 7bit\r\n\r\n`,
    `${message}\r\n\r\n`,
    `--message_boundary\r\n`,
    `Content-Type: application/pdf\r\n`,
    `MIME-Version: 1.0\r\n`,
    `Content-Disposition: attachment; filename="${pdfFileName}.pdf"\r\n\r\n`,
    pdfData,
    `\r\n\r\n`,
    `--message_boundary--`,
  ].join("");
  const raw = btoa(body)
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=+$/, "");
  const request = gmail.users.messages.send({
    userId: "me",
    resource: { raw },
  });
  request.execute(() => {
    console.log(`sent email to ${recipient} with subject : ${subject}`);
    done && done();
  });
};

/**
 * get the link for the feedback form.
 */
const getFeedbackFormLink = async (baseFolder, kvp) => {
  // find out the Feedback form link and add it to the kvp
  const q = `name = 'Feedback' and trashed = false and parents in '${baseFolder}'`;
  const {
    result: { files },
  } = await drive.files.list({ q });
  const sharableLink = await getSharableLink(files[0].id);
  if (!kvp) return sharableLink;
  const details = `${kvp["session_name"]} has raised a dispute for session on ${kvp["session_date"]} at ${kvp["session_address"]}`;
  return `${sharableLink.substring(
    0,
    sharableLink.lastIndexOf("/")
  )}/viewform?usp=pp_url&entry.1196138576=${
    kvp["type"] ? kvp["type"] : "Dispute"
  }&entry.1860268951=${
    kvp["session_id"]
  }&entry.445580806=${details}&entry.584140625=${kvp["session_mobile"]} / ${
    kvp["session_email"]
  }`;
};

/**
 * for generating an invoice, we do the following:
 * 1. copy an google-docs template from a public source to the specified file in user's drive
 * 2. do a search / replace of keywords in the template
 * 3. return a viewable link
 */
const generateInvoice = async (filePath, kvp) => {
  const feedbackFormLink = await getFeedbackFormLink(filePath.split("/")[0]);
  kvp["feedback_link"] = feedbackFormLink;
  // create a copy of the template
  const { id, webViewLink } = await copyAsset(invoiceTemplateId, filePath);
  // then do a search / replace of all KVPs
  const requests = Object.entries(kvp).map(([key, value]) => ({
    replaceAllText: {
      replaceText: value,
      containsText: { text: key.toUpperCase(), matchCase: true },
    },
  }));
  await documents.batchUpdate({ documentId: id }, { requests });
  console.log(`generated PDF at ${webViewLink}`);
  return { id, webViewLink, feedbackFormLink };
};

/**
 * init a new patient by copying a bunch of files into the patient's shared drive
 */
const initNewPatient = async (name, folderId) => {
  await copyAsset(feedbackTemplateId, `${folderId}/Feedback`);
};

/**
 * ensure that we have SPOT_DATA and SPOT_DATA/PATIENT_DATA and SPOT_DATA/PATIENT_DATA/settings in place.
 * @returns true if settings was existing already, false otherwise
 */
const initGoogleDrive = async () => {
  // sleep until the api is ready
  const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
  while (
    !window.gapi.client.drive ||
    !window.gapi.client.docs ||
    !window.gapi.client.sheets ||
    !window.gapi.client.gmail
  )
    await sleep(1000);
  drive = window.gapi.client.drive;
  spreadsheets = window.gapi.client.sheets.spreadsheets;
  documents = window.gapi.client.docs.documents;
  gmail = window.gapi.client.gmail;

  // first we find a folder named SPOT_DATA in Google Drive. if not found, we create the entire structure
  spotDataFolderId = await findAsset("SPOT_DATA", true);
  if (!spotDataFolderId) {
    // create SPOT_DATA folder and a sheet called settings
    spotDataFolderId = await createAsset(FOLDER, "SPOT_DATA", null, true);
    settingsSpreadsheetId = await createAsset(SHEET, "settings");
    // copy all the templates from a central place
    templatesFolderId = await copyFolder(
      starterDocsFolder,
      `${spotDataFolderId}/templates`
    );
    // note the location of the invoiceTemplate and feedbackTemplateId
    invoiceTemplateId = await findAsset(`${templatesFolderId}/Invoice`);
    feedbackTemplateId = await findAsset(`${templatesFolderId}/Feedback`);
    return false;
  }

  // if SPOT_DATA folder is found, we retrieve all of the patients records (a folder for each patient)
  for (const { name, id } of await listFolder(spotDataFolderId, FOLDER)) {
    if (name === "templates") {
      templatesFolderId = id;
      // note the location of invoiceTemplate and feedbackTemplateId
      invoiceTemplateId = await findAsset(`${templatesFolderId}/Invoice`);
      feedbackTemplateId = await findAsset(`${templatesFolderId}/Feedback`);
    } else
      patientFolders[name] = {
        folderId: id,
        recordsSpreadsheetId: await findAsset(`${id}/records`),
      };
  }

  // also, we look for settings sheet in it
  settingsSpreadsheetId = await findAsset("settings");
  if (!settingsSpreadsheetId) {
    settingsSpreadsheetId = createAsset(SHEET, "settings");
    return false;
  }

  // if settings are found, return it
  settings = await getSpreadsheet(settingsSpreadsheetId);
  return settings;
};

export {
  initGoogleDrive,
  createAsset,
  renameAsset,
  getSpreadsheet,
  saveSpreadsheet,
  settingsSpreadsheetId,
  setSettingsSpreadsheetId,
  invoiceTemplateId,
  patientFolders,
  sendEmail,
  generateInvoice,
  shareAsset,
  getSharableLink,
  downloadAsPDF,
  spotDataFolderId,
  initNewPatient,
  getFeedbackFormLink,
};
