import React from 'react';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Loader from '../components/Loader';
import Icon from '@mdi/react';
import { mdiCheck } from '@mdi/js';
import { mdiHandPointingLeft } from '@mdi/js';

import { useGlobalState } from '../store';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    landingMessages: {
        display: 'block',
        margin: '1rem'
    },
    icon: {
        verticalAlign: 'middle'
    },
    processing: {
        height: '1.5rem',
        verticalAlign: 'middle'
    },
    time: {
        opacity: 0.7,
        padding: '0 0.5rem'
    },
    newPatientArrow: {
        position: 'fixed',
        left: '3rem',
        top: '8.5rem',
        zIndex: 111111
    }
}));

function LandingPage(props) {
    const classes = useStyles();

    const [ loading ] = useGlobalState('loading');
    const [ profile ] = useGlobalState('profile');
    const [ events ] = useGlobalState('events');
    const [ , setHeading ] = useGlobalState('heading');
    const [ settingsFound ] = useGlobalState('settingsFound');
    const [ , setOpenSpreadsheetId ] = useGlobalState('openSpreadsheetId');

    profile && setHeading(profile.name);
    setOpenSpreadsheetId(null);

    return (
        <div className={classes.root}>
            <div className={classes.landingMessages}>
                {loading && <Loader />}
                {!loading &&
                !settingsFound && (
                    <div>
                        <Typography paragraph>Welcome to SPOT</Typography>
                        <Typography paragraph>
                            It seems like this is your first visit. Click SETTINGS to fill-in your details
                        </Typography>
                    </div>
                )}
                {!loading &&
                settingsFound &&
                events.length > 0 && (
                    <div>
                        <Typography variant="caption" style={{ color: 'rgba(0,0,0,0.8)' }}>
                            Since you last loggedin ...
                        </Typography>
                        {/* display all events */}
                        <table>
                            <tbody>
                                {events.map(({ key, timestamp, name, type, processed }) => {
                                    const time = new Date(timestamp).toLocaleDateString('en-US', {
                                        month: 'short',
                                        day: 'numeric'
                                    });
                                    return (
                                        <tr key={key}>
                                            <td>
                                                {!processed && (
                                                    <img
                                                        src="/img/loading_red.gif"
                                                        alt=""
                                                        className={classes.processing}
                                                    />
                                                )}
                                                {processed && (
                                                    <Icon
                                                        path={mdiCheck}
                                                        color={'#4285f4'}
                                                        size={1}
                                                        className={classes.icon}
                                                    />
                                                )}
                                            </td>
                                            <td style={{ minWidth: '4rem' }}>
                                                <Typography variant="body1">
                                                    <span className={classes.time}>{time}</span>
                                                </Typography>
                                            </td>
                                            <td>
                                                <Typography variant="body1" style={{ margin: '1rem 0' }}>
                                                    {type === 'NEW_PATIENT_ADDED' && `A new patient was added`}
                                                </Typography>
                                                {type === 'NEW_PATIENT_ADDED' && (
                                                    <Icon
                                                        path={mdiHandPointingLeft}
                                                        color={'#ea002d'}
                                                        size={1.5}
                                                        className={`${classes.icon} ${classes.newPatientArrow}`}
                                                    />
                                                )}
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                )}
            </div>
        </div>
    );
}

export default LandingPage;
