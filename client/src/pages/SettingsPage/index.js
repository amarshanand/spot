import React from 'react';
import { useGlobalState } from '../../store';

import Form from './Form';

function SettingsPage(props) {
	const [ , setSettingsFound ] = useGlobalState('settingsFound');
	const [ profile, setProfile ] = useGlobalState('profile');
	const values = {
		name: profile.name,
		qualifications: '',
		speciality: '',
		phone: '',
		address: '',
		altEmails: '',
		ABN: '',
		providerNo: '',
		bank: '',
		accountName: '',
		BSB: '',
		accountNo: ''
	};
	const styles = {
		name: { order: 1, width: '16rem' },
		qualifications: { order: 2, width: '16rem' },
		speciality: { order: 3, width: '16rem' },
		address: { order: 4, width: '16rem' },
		altEmails: { order: 5, width: '16rem' },
		phone: { order: 6, width: '8rem', type: 'tel' },
		ABN: { order: 7, width: '7rem' },
		providerNo: { order: 8, width: '6rem' },
		bank: { order: 9, width: '12rem' },
		accountName: { order: 10, width: '16rem' },
		BSB: { order: 11, width: '4rem' },
		accountNo: { order: 12, width: '8rem' }
	};
	return (
		<Form
			fields={values}
			styles={styles}
			heading={'Settings'}
			fileName={`settings`}
			onSave={(id, folderId, _values) => {
				setSettingsFound(true);
				setProfile({ ...profile, name: _values.name });
			}}
		/>
	);
}

export default SettingsPage;
