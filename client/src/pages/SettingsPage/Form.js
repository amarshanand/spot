import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import SaveIcon from '@material-ui/icons/Save';
import Loader from '../../components/Loader';

import { useGlobalState } from '../../store';
import { getSpreadsheet, saveSpreadsheet } from '../../api/sheets';

/**
 * The Form component for Settings Page
 */

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		margin: '1rem auto'
	},
	form: {
		marginTop: '-1.75rem'
	},
	textField: {
		width: '30%',
		margin: '1rem'
	},
	save: {
		margin: theme.spacing.unit,
		position: 'fixed',
		bottom: '1rem',
		right: '1rem'
	}
}));

function Form({ fields, styles, heading, fileName, folderName, onSave }) {
	const classes = useStyles();
	const [ openSpreadsheetId, setOpenSpreadsheetId ] = useGlobalState('openSpreadsheetId');
	const [ loading, setLoading ] = useState(false);
	const [ saving, setSaving ] = useState(false);
	const [ values, setValues ] = useState(fields);
	const [ , setHeading ] = useGlobalState('heading');
	const [ , setNotification ] = useGlobalState('notification');
	setHeading(heading);

	// on loading the component, if its not a new sheet, load the values from the sheet
	useEffect(
		() => {
			if (!openSpreadsheetId) return setValues(fields);
			setLoading(true);
			getSpreadsheet(openSpreadsheetId).then((_values) => {
				setLoading(false);
				setValues({ ...values, ..._values });
			});
		},
		[ openSpreadsheetId ]
	);

	// replace {name} with value from values
	const resolve = (text) => text.replace(/{(.*?)}/g, (match, group) => values[group]);

	// update the state as the user types in
	const handleChange = (name) => (event) => setValues({ ...values, [name]: event.target.value });

	// upon saving the form, update the spreadsheet, and call the onSave() handler subsequently
	const doSave = async () => {
		try {
			setSaving(true);
			const resolvedFileName = resolve(fileName).replace(/ /g, '_');
			const resolvedFolderName = folderName ? resolve(folderName).replace(/ /g, '_') : null;
			const { id, folderId } = await saveSpreadsheet(
				openSpreadsheetId,
				Object.entries(values),
				resolvedFileName,
				resolvedFolderName
			);
			setOpenSpreadsheetId(id);
			setSaving(false);
			setNotification({ text: `${heading} Saved`, type: 'success' });
			onSave && onSave(id, folderId, values, !openSpreadsheetId);
		} catch (e) {
			setSaving(false);
			console.log(`Could not save : ${e}`);
			setNotification({ text: 'Could not save', type: 'error' });
		}
	};

	// a single editable field with title and contents
	const getEditable = (key, value, style) => {
		// hidden things
		if (!style || style.hidden) return <span id={key} key={key} />;
		return (
			<TextField
				id={key}
				key={key}
				label={key.split('~')[0]}
				className={classes.textField}
				value={Boolean(value) ? value : ''}
				onChange={handleChange(key)}
				margin="normal"
				type={style.type || 'text'}
				disabled={style.readonly || saving}
				style={{ ...style }}
			/>
		);
	};

	// starting from a list of nvps, look into the styles for each name, and return an array of keys sorted by 'order'
	const keysSortedByOrder = (nvps, _styles = styles) => {
		return Object.entries(nvps)
			.sort((a, b) => {
				if (Array.isArray(a[1])) return a;
				if (Array.isArray(b[1])) return b;
				if (!_styles[a[0]] || !_styles[b[0]]) {
					console.log(`WARNING : Missing field from style : a=${a} and b=${b} and _styles=`);
					console.dir(_styles);
					debugger;
					return 0;
				}
				return _styles[a[0]].order - _styles[b[0]].order;
			})
			.map((e) => e[0])
			.filter((e) => _styles[e] && !_styles[e].hidden);
	};

	if (!styles) styles = {};
	return (
		<div className={classes.root}>
			{(loading || saving) && <Loader />}
			{!loading && (
				<form className={classes.form} noValidate autoComplete="off">
					{keysSortedByOrder(values).map((key) => getEditable(key, values[key], styles[key]))}
					<Fab
						color="primary"
						aria-label="Save"
						className={`${saving ? 'rotate' : ''} ${classes.save}`}
						disabled={saving}
						onClick={doSave}
					>
						<SaveIcon />
					</Fab>
				</form>
			)}
		</div>
	);
}

export default Form;
