import React from "react";
import { makeStyles } from "@material-ui/styles";

import { useGlobalState } from "../store";
import { initGoogleDrive, patientFolders, spotDataFolderId } from "../api/sheets";

const useStyles = makeStyles(() => ({
  root: {},
  topbar: {
    display: "flex",
    justifyContent: "space-between",
    padding: "2rem 3rem",
    background: "rgb(70, 131, 255)",
    marginBottom: "2rem",
  },
  logo: { width: "8rem", height: "2rem" },
  intro: {
    paddingTop: "56.25%",
    margin: "0 1rem 1rem 1rem",
    background: "rgb(0,0,0)",
    textAlign: "center",
    position: "relative",
  },
  iframe: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
  },
}));

function LoginPage(props) {
  const classes = useStyles();
  const [, setProfile] = useGlobalState("profile");
  const [, setCurrPage] = useGlobalState("currPage");
  const [, setSettingsFound] = useGlobalState("settingsFound");
  const [, setPatients] = useGlobalState("patients");
  const [, setLoading] = useGlobalState("loading");
  const [, setSpotDataFolderId] = useGlobalState("spotdataFolderFolderId");
  const [, setNotification] = useGlobalState("notification");

  setLoading(true);
  window.onSignIn = async (googleUser) => {
    let _profile = googleUser.getBasicProfile();
    // we only accept gmail addresses
    if (!_profile.getEmail().includes("@gmail.com")) {
      setNotification({
        text: "SPOT can only be used with gmail addresses",
        type: "error",
      });
      return window.gapi.auth2.getAuthInstance().signOut();
    }
    setCurrPage("LANDING_PAGE");
    // initGoogleDrive() syncs the app with the user's Google Drive
    const settings = await initGoogleDrive();
    setSettingsFound(Boolean(settings));
    _profile = {
      email: _profile.getEmail(),
      name: _profile.getName(),
      imageUrl: _profile.getImageUrl(),
    };
    if (settings) _profile = { ..._profile, ...settings }; // overwrite fields from settings
    setProfile(_profile);
    // obtain a list of all patient folders and set them on the side bar
    patientFolders &&
      setPatients(
        Object.entries(patientFolders).map(([name, { recordsSpreadsheetId, folderId }]) => {
          return {
            name: name.substring(7),
            color: `#${name.substring(0, 6)}`,
            recordsSpreadsheetId,
            folderId,
          };
        })
      );
    setLoading(false);
    setSpotDataFolderId(spotDataFolderId);
  };

  return (
    <div className={classes.root}>
      <div className={classes.topbar}>
        <img alt="spot logo" className={classes.logo} src="img/spot.png" />
        <div className="g-signin2" data-onsuccess="onSignIn" />
      </div>
      <div className={classes.intro} style={{ padding: 0, marginBottom: "2rem" }}>
        {
          <iframe
            width={window.screen.width * 0.9}
            height={(window.screen.width * 315 * 0.9) / 560}
            src="https://www.youtube.com/embed/sV_gI2pPX7Y"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        }
      </div>
      <div className={classes.intro}>
        <iframe
          title="SPOT presentattion"
          style={{
            background: "rgb(104,102,98)",
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
          }}
          src="https://docs.google.com/presentation/d/e/2PACX-1vQTlmORtxSzKuRYJ05R2KBY9u7kOt2ds5HpxzpSEGFhCI6GTahvyX_-yrAC8uZ2vOmprcXos8OVbgIa/embed?start=false&loop=false&delayms=3000"
          frameBorder="0"
          allowFullScreen="true"
          mozallowFullScreen="true"
          webkitallowFullScreen="true"
        />
      </div>
    </div>
  );
}

export default LoginPage;
