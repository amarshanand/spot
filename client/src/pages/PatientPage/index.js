import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Form from "./Form";
import SessionList from "./SessionList";
import {
  sendNewInvoice,
  sendNewReceipt,
  sendExistingInvoice,
  sendExistingReceipt,
} from "./Communications";
import { useGlobalState } from "../../store";
import {
  getSpreadsheet,
  saveSpreadsheet,
  shareAsset,
  renameAsset,
  initNewPatient,
} from "../../api/sheets";

const useStyles = makeStyles((theme) => ({
  tabBar: {
    background: "rgba(0,0,0,0.05)",
    margin: "-1.5rem 0 3rem -1.5rem",
    width: "calc(100% + 3rem)",
  },
}));

const initialValues = {
  name: "",
  mobile: "",
  email: "",
  address: "",
  color: "#" + Math.floor(Math.random() * 16777215).toString(16),
  sessions: [
    {
      date: "",
      goals: "",
      techniques: "",
      comments: "",
      followup: "",
      notes: "",
      code: "",
      description: "",
      id: "",
      duration: "",
      cost: "",
      travel_details: "",
      travel_time: "",
      travel_cost: "",
      invoiceId: "",
      invoiceLink: "",
      receiptId: "",
      receiptLink: "",
      status: "",
    },
  ],
};
// this will be passed to the individual textfield
const styles = {
  name: { order: 1, width: "15rem" },
  mobile: { order: 2, width: "10rem" },
  email: { order: 3, width: "15rem", type: "email" },
  address: { order: 4, width: "15rem" },
  color: { order: 5, width: "0rem", hidden: true },
  status: { order: 6, width: "10rem", readonly: true },
};

function PatientPage() {
  const classes = useStyles();
  const [patients, setPatients] = useGlobalState("patients");
  const [openSpreadsheetId, setOpenSpreadsheetId] = useGlobalState(
    "openSpreadsheetId"
  );
  const [openSpreadsheetFolderId, setOpenSpreadsheetFolderId] = useGlobalState(
    "openSpreadsheetFolderId"
  );
  const [profile] = useGlobalState("profile");
  const [, setHeading] = useGlobalState("heading");
  const [, setCurrPage] = useGlobalState("currPage");
  const [events, setEvents] = useGlobalState("events");
  const [showSession] = useGlobalState("showSession");
  const [tabValue, setTabValue] = useState(Boolean(openSpreadsheetId) ? 0 : 1);
  const [values, setValues] = useState(initialValues);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!openSpreadsheetId) {
      setTabValue(1);
      initialValues.color =
        "#" + Math.floor(Math.random() * 16777215).toString(16);
      setValues(initialValues);
      return;
    }
    const update = async () => {
      setLoading(true);
      const _values = await getSpreadsheet(openSpreadsheetId);
      // if no session has been stored before, put a blank session to clone from
      if (_values.sessions.length <= 0)
        _values.sessions.push(initialValues.sessions[0]);
      setLoading(false);
      setValues(_values);
    };
    update();
  }, [openSpreadsheetId]);

  const onSave = async (valuesFromForm, type, columns, done) => {
    let updatedValues = {};
    if (type === "info") updatedValues = { ...values, ...valuesFromForm };
    else {
      let sessionIndex = values.sessions.findIndex(
        ({ id }) => id === valuesFromForm.id
      );
      if (sessionIndex < 0)
        updatedValues = {
          ...values,
          sessions: [valuesFromForm, ...values.sessions],
        };
      else {
        values.sessions[sessionIndex] = {
          ...values.sessions[sessionIndex],
          ...valuesFromForm,
        };
        updatedValues = { ...values };
      }
    }
    updatedValues.sessions = updatedValues.sessions.filter(
      ({ id }) => id !== ""
    ); // remove blanks
    const folderName = `${updatedValues.color.substring(1)} ${
      updatedValues.name
    }`;
    if (openSpreadsheetFolderId)
      await renameAsset(openSpreadsheetFolderId, folderName);
    const { id, folderId } = await saveSpreadsheet(
      openSpreadsheetId,
      Object.entries(updatedValues),
      "records",
      folderName,
      columns
    );
    // if a new patient has just been created, share the folder
    if (!openSpreadsheetId) {
      patients.unshift({
        name: updatedValues.name,
        color: updatedValues.color,
        recordsSpreadsheetId: id,
        folderId,
      });
      setPatients([...patients]);
      await initNewPatient(updatedValues.name, folderId);
      await shareAsset(
        folderId,
        updatedValues.email,
        `Dear ${updatedValues.name}. I have created a shared space for storing all of your session records and invoices. Please click the Open button to view the shared space. Regards ${profile.name}`
      );
    }
    setOpenSpreadsheetId(id);
    setOpenSpreadsheetFolderId(folderId);
    setHeading(updatedValues.name);
    setValues(updatedValues);
    done && done();
  };

  const onInvoice = async (session, columns) => {
    if (["INVOICED", "PAID"].includes(session.status)) {
      await sendExistingInvoice(
        { ...info, ...session },
        profile,
        openSpreadsheetFolderId
      );
      return info.name;
    }
    session.status = "INVOICED";
    const { invoiceId, invoiceLink } = await sendNewInvoice(
      { ...info, ...session },
      profile,
      openSpreadsheetFolderId
    );
    const index = values.sessions.findIndex((s) => s.id === session.id);
    values.sessions[index].invoiceId = invoiceId;
    values.sessions[index].invoiceLink = invoiceLink;
    await saveSpreadsheet(
      openSpreadsheetId,
      Object.entries(values),
      "records",
      `${info.color.substring(1)} ${info.name}`,
      columns
    );
    return info.name;
  };

  const onSendReceipt = async (session, columns) => {
    if (["PAID"].includes(session.status)) {
      await sendExistingReceipt(
        { ...info, ...session },
        profile,
        openSpreadsheetFolderId
      );
      return info.name;
    }
    session.status = "PAID";
    const { receiptId, receiptLink } = await sendNewReceipt(
      { ...info, ...session },
      profile,
      openSpreadsheetFolderId
    );
    const index = values.sessions.findIndex((s) => s.id === session.id);
    values.sessions[index].receiptId = receiptId;
    values.sessions[index].receiptLink = receiptLink;
    await saveSpreadsheet(
      openSpreadsheetId,
      Object.entries(values),
      "records",
      `${info.color.substring(1)} ${info.name}`,
      columns
    );
    return info.name;
  };

  const onTabChange = (event, newValue) => setTabValue(newValue);

  // separate patient-info from session-info
  let { sessions, ...info } = values;
  let { sessions: sessionStyles, ...infoStyles } = styles;
  // remove any blank sessions, except when its the first (seed) session
  if (sessions.length > 1) sessions = sessions.filter(({ id }) => id !== "");
  // this is a cheap hack. for some reason, info is getting reset to {}. to avoid a blank page, we redirect the user to Home
  if (Object.keys(info).length <= 0) {
    // we send an event that tells that a new user was added
    setEvents([
      {
        key: Math.random(),
        date: Date.now(),
        type: "NEW_PATIENT_ADDED",
        processed: true,
      },
      ...events,
    ]);
    setCurrPage("LANDING_PAGE");
  }

  // show home-page, which is either a list-of-sessions or patient-info
  return (
    <div>
      {!showSession && (
        <Tabs
          className={classes.tabBar}
          value={tabValue}
          onChange={onTabChange}
          variant="fullWidth"
        >
          {<Tab label="Sessions" disabled={!Boolean(openSpreadsheetId)} />}
          <Tab label="Profile" disabled={Boolean(showSession)} />
        </Tabs>
      )}
      {/* list of sessions */}
      {tabValue === 0 && (
        <SessionList
          name={values.name}
          loading={loading}
          sessions={sessions}
          onSave={onSave}
          onInvoice={onInvoice}
          onSendReceipt={onSendReceipt}
        />
      )}
      {/* patient info */}
      {tabValue === 1 && (
        <Form
          loading={loading}
          fields={info}
          styles={infoStyles}
          onSave={(v, columns) => onSave(v, "info", columns)}
        />
      )}
    </div>
  );
}

export default PatientPage;
