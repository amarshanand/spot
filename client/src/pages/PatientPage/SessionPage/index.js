import React from "react";
import Form from "./Form";

const initialValues = {
  date: "",
  location: "",
  goals: "",
  techniques: "",
  comments: "",
  followup: "",
  notes:"",
  code: "",
  description: "",
  id: "",
  duration: 0,
  cost: 0,
  travel_details: "",
  travel_time: 0,
  travel_cost: 0,
  status: "",
  invoiceId: "",
  invoiceLink: "",
  receiptId: "",
  receiptLink: "",
};
// this will be passed to the individual textfield
const sessionStyles = {
  id: { order: 1, width: "4rem" },
  date: { order: 2, width: "9.5rem" },
  location: { order: 3, width: "15rem" },
  description: { order: 4, width: "15rem" },
  code: { order: 5, width: "3rem" },
  duration: { order: 6, width: "4rem" },
  cost: { order: 7, width: "4rem", type: "number" },
  travel_details: { order: 8, width: "15rem" },
  travel_time: { order: 9, width: "6rem" },
  travel_cost: { order: 10, width: "5rem", type: "number" },
  goals: { order: 11, width: "15rem", noOfLines: 6 },
  techniques: { order: 12, width: "15rem", noOfLines: 6 },
  comments: { order: 13, width: "15rem", noOfLines: 6 },
  followup: { order: 14, width: "15rem", noOfLines: 6 },
  notes: { order: 15, width: "15rem", noOfLines: 6 },
  invoiceId: { order: 16, width: "6rem", hidden: true },
  invoiceLink: { order: 17, width: "6rem", link: true, text: "Invoice" },
  receiptId: { order: 18, width: "6rem", hidden: true },
  receiptLink: { order: 19, width: "6rem", link: true, text: "Receipt" },
  status: { order: 20, width: "6rem", caption: true },
};

function SessionPage({ fields, onSave, onClose, onInvoice, onSendReceipt }) {
  return (
    <Form
      fields={fields ? fields : initialValues}
      styles={sessionStyles}
      onClose={onClose}
      onSave={(kvp, type, columns, done) => {
        kvp.status = "SAVED";
        onSave(kvp, type, columns, done);
      }}
      onInvoice={onInvoice}
      onSendReceipt={onSendReceipt}
    />
  );
}

export default SessionPage;
