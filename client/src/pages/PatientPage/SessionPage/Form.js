import React, { useState } from "react";
import { makeStyles } from "@material-ui/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import SaveIcon from "@material-ui/icons/Save";
import CloseIcon from "@material-ui/icons/Cancel";
import Icon from "@mdi/react";
import { mdiFileDocumentBox, mdiFileDocumentBoxCheck } from "@mdi/js";
import Loader from "../../../components/Loader";
import { useGlobalState } from "../../../store";
import { sendEmail } from "../../../api/sheets";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    margin: "1rem -1rem 0 -1rem",
    minHeight: "30rem",
  },
  form: {
    marginTop: "-1.75rem",
  },
  textField: {
    margin: "1rem",
  },
  link: {
    verticalAlign: "bottom",
    margin: "1.5rem 0.5rem 1rem",
  },
  caption: {
    color: "#888888",
    letterSpacing: "2px",
    margin: "1rem",
    display: "inline-block",
    verticalAlign: "bottom",
  },
  save: {
    margin: theme.spacing.unit,
    position: "fixed",
    bottom: "9rem",
    right: "1rem",
  },
  invoice: {
    margin: theme.spacing.unit,
    position: "fixed",
    bottom: "5rem",
    right: "1rem",
  },
  receipt: {
    margin: theme.spacing.unit,
    position: "fixed",
    bottom: "1rem",
    right: "1rem",
  },
  close: {
    position: "fixed",
    top: "4.5rem",
    right: "1.1rem",
    width: "2rem",
    height: "2rem",
    color: "red",
    boxShadow: "1px 1px 6px 0px rgba(0,0,0,0.5)",
    borderRadius: "50%",
  },
  closeIcon: {
    width: "auto",
    height: "auto",
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
}));

function Form({
  fields,
  styles,
  onSave,
  onInvoice,
  onSendReceipt,
  onClose,
  loading,
}) {
  const classes = useStyles();
  const [saving, setSaving] = useState(false);
  const [invoicing, setInvoicing] = useState(false);
  const [sendingReceipt, setSendingReceipt] = useState(false);
  const [values, setValues] = useState(fields);
  const [heading, setHeading] = useGlobalState("heading");
  const [, setNotification] = useGlobalState("notification");
  const [, setConfirm] = useGlobalState("confirm");
  setHeading(fields.date);

  // update the state as the user types in. for first level elements, its simple. for second level, the name contains the full path
  const handleChange = (name) => (event) =>
    setValues({ ...values, [name]: event.target.value });

  const doSave = async () => {
    try {
      setSaving(true);
      await onSave(values, "sessions", sortedKeys, () => {
        setSaving(false);
        setNotification({
          text: `Session on ${heading} Saved`,
          type: "success",
        });
      });
    } catch (e) {
      setSaving(false);
      console.log(`Could not save : ${e}`);
      setNotification({ text: "Could not save", type: "error" });
    }
  };

  const doInvoice = async () => {
    try {
      setInvoicing(true);
      const recepient = await onInvoice(values, sortedKeys);
      setInvoicing(false);
      setNotification({
        text: `Invoice sent to ${recepient}`,
        type: "success",
      });
    } catch (e) {
      setInvoicing(false);
      console.log(`Could not send Invoice : ${JSON.stringify(e, null, 2)}`);
      sendEmail(
        `amarsh.anand@gmail.com`,
        `SPOT error while sending invoice`,
        `<p><b>values</b><br/><pre><code>${JSON.stringify(
          values,
          null,
          2
        )}</code></pre></p>` +
          `<p><b>sortedKeys</b><br/><pre><code>${JSON.stringify(
            sortedKeys,
            null,
            2
          )}</code></pre></p>` +
          `<p><b>error</b><br/><pre><code>${JSON.stringify(
            e,
            null,
            2
          )}</code></pre></p>`
      );
      setNotification({ text: "Could not send Invoice", type: "error" });
    }
  };

  const doSendReceipt = async () => {
    try {
      setSendingReceipt(true);
      const recepient = await onSendReceipt(values, sortedKeys);
      setSendingReceipt(false);
      setNotification({
        text: `Receipt sent to ${recepient}`,
        type: "success",
      });
    } catch (e) {
      setSendingReceipt(false);
      console.log(`Could not send Receipt : ${JSON.stringify(e, null, 2)}`);
      sendEmail(
        `amarsh.anand@gmail.com`,
        `SPOT error while sending receipt`,
        `<p><b>values</b><br/><pre><code>${JSON.stringify(
          values,
          null,
          2
        )}</code></pre></p>` +
          `<p><b>sortedKeys</b><br/><pre><code>${JSON.stringify(
            sortedKeys,
            null,
            2
          )}</code></pre></p>` +
          `<p><b>error</b><br/><pre><code>${JSON.stringify(
            e,
            null,
            2
          )}</code></pre></p>`
      );
      setNotification({ text: "Could not send Receipt", type: "error" });
    }
  };

  // a single editable field with title and contents
  const getEditable = (key, value, style) => {
    // hidden things
    if (!style || style.hidden) return <span id={key} key={key} />;
    // links are buttons for us
    if (style.link) {
      if (value === "") return <span id={key} key={key} />;
      return (
        <Button
          id={key}
          key={key}
          className={classes.link}
          variant="outlined"
          onClick={() => window.open(value, "_blank")}
        >
          {style.text ? style.text : key.split("~")[0].replace(/_/gi, " ")}
        </Button>
      );
    }
    if (style.caption)
      return (
        <Typography id={key} key={key} className={classes.caption} variant="h6">
          {value}
        </Typography>
      );
    // multiline textarea
    if (style.noOfLines > 1)
      return (
        <TextField
          id={key}
          key={key}
          label={
            style.text ? style.text : key.split("~")[0].replace(/_/gi, " ")
          }
          className={classes.textField}
          multiline
          rows={style.noOfLines}
          variant="outlined"
          value={Boolean(value) ? value : ""}
          onChange={handleChange(key)}
          margin="normal"
          type={style.type || "text"}
          disabled={
            style.readonly ||
            Boolean(saving || invoicing || sendingReceipt) ||
            ["PAID", "INVOICED"].includes(values.status)
          }
          style={{ ...style }}
        />
      );
    // default to single line editable text field
    return (
      <TextField
        id={key}
        key={key}
        label={style.text ? style.text : key.split("~")[0].replace(/_/gi, " ")}
        className={classes.textField}
        value={Boolean(value) ? value : ""}
        onChange={handleChange(key)}
        margin="normal"
        type={style.type || "text"}
        disabled={
          style.readonly ||
          Boolean(saving || invoicing || sendingReceipt) ||
          ["PAID", "INVOICED"].includes(values.status)
        }
        style={{ ...style }}
      />
    );
  };

  // starting from a list of nvps, look into the styles for each name, and return an array of keys sorted by 'order'
  const keysSortedByOrder = (nvps, _styles = styles) => {
    const keys = Object.entries(nvps)
      .sort((a, b) => {
        if (Array.isArray(a[1])) return a;
        if (Array.isArray(b[1])) return b;
        if (!_styles[a[0]] || !_styles[b[0]]) {
          console.log(
            `WARNING : Missing field from style : a=${a} and b=${b} and _styles=`
          );
          console.dir(_styles);
          debugger;
          return 0;
        }
        return _styles[a[0]].order - _styles[b[0]].order;
      })
      .map((e) => e[0]);
    return keys;
  };
  const sortedKeys = keysSortedByOrder(values);
  if (!styles) styles = {};
  return (
    <div className={classes.root}>
      {(loading || saving || invoicing || sendingReceipt) && <Loader />}
      {!loading && (
        <form className={classes.form} noValidate autoComplete="off">
          {sortedKeys.map((key) => getEditable(key, values[key], styles[key]))}
          {onClose && (
            <div className={classes.close} onClick={onClose}>
              <CloseIcon className={classes.closeIcon} />
            </div>
          )}
          {/* Send invoice */}
          {onInvoice && (
            <Fab
              size="small"
              color="primary"
              aria-label="Invoice"
              className={`${invoicing ? "rotate" : ""} ${classes.invoice}`}
              disabled={
                Boolean(saving || invoicing || sendingReceipt) ||
                ["UNSAVED"].includes(values.status)
              }
              onClick={() => {
                setConfirm({
                  show: true,
                  text: `Would you like to email an invoice?`,
                  ok: doInvoice,
                });
              }}
            >
              <Icon path={mdiFileDocumentBox} color="white" size={1} />
            </Fab>
          )}
          {/* Send receipt */}
          {onSendReceipt && (
            <Fab
              size="small"
              color="primary"
              aria-label="Send Recepit"
              className={`${sendingReceipt ? "rotate" : ""} ${classes.receipt}`}
              disabled={
                Boolean(saving || invoicing || sendingReceipt) ||
                ["SAVED", "UNSAVED"].includes(values.status)
              }
              onClick={() => {
                setConfirm({
                  show: true,
                  text: `Would you like to email a receipt?`,
                  ok: doSendReceipt,
                });
              }}
            >
              <Icon path={mdiFileDocumentBoxCheck} color="white" size={1} />
            </Fab>
          )}
          {/* Save session */}
          <Fab
            size="small"
            color="primary"
            aria-label="Save"
            className={`${saving ? "rotate" : ""} ${classes.save}`}
            disabled={
              Boolean(saving || invoicing || sendingReceipt) ||
              ["INVOICED", "PAID"].includes(values.status)
            }
            onClick={doSave}
          >
            <SaveIcon />
          </Fab>
        </form>
      )}
    </div>
  );
}

export default Form;
