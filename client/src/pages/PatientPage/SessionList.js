import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Typography } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import Loader from '../../components/Loader';
import AddIcon from '@material-ui/icons/Add';
import SessionPage from './SessionPage';
import { useGlobalState } from '../../store';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		margin: '1rem auto'
	},
	form: {
		margin: '-4.5rem 0 0 -3rem',
		width: '100%'
	},
	add: {
		margin: theme.spacing.unit,
		position: 'fixed',
		bottom: '1rem',
		right: '1rem'
	},
	table: {
		margin: '2.5rem',
		borderCollapse: 'collapse',
		width: 'calc(100% + 0.9rem)'
	},
	cell: {
		padding: '1rem',
		position: 'relative',
		borderBottom: '1px solid rgb(0,0,0,0.1)'
	}
}));

function SessionList({ name, sessions, loading, onSave, onInvoice, onSendReceipt }) {
	const classes = useStyles();
	const [ saving ] = useState(false);
	const [ showSession, setShowSession ] = useGlobalState('showSession');
	const [ heading, setHeading ] = useGlobalState('heading');

	// a sequence of similar looking data (rows). we can add / remove rows from the table
	//console.log(`sessions in SessionList`);
	//console.dir(sessions);
	const SessionTable = () => (
		<table className={classes.table}>
			<tbody>
				{sessions.map(
					({ date, title, status }, index) =>
						date !== '' && (
							<tr key={index} onClick={() => setShowSession(sessions[index])}>
								<td className={classes.cell} style={{ textAlign: 'left' }}>
									<Typography>{date.split(',')[0]}</Typography>
								</td>
								<td className={classes.cell} style={{ textAlign: 'right' }}>
									<Typography>{status}</Typography>
								</td>
							</tr>
						)
				)}
			</tbody>
		</table>
	);

	const generateSessionId = () => {
		const toks = heading.toUpperCase().split(' ');
		if (toks.length === 1) toks.push(toks[0]);
		if (sessions[0].id === '') return `${toks[0][0]}${toks[1][0]}001`;
		let sessionNo = sessions.length + 1;
		sessionNo = sessionNo < 10 ? `00${sessionNo}` : sessionNo < 100 ? `0${sessionNo}` : `${sessionNo}`;
		//const sessionNo = Math.floor(Math.random() * (999 - 100 + 1) + 100);
		return `${toks[0][0]}${toks[1][0]}${sessionNo}`;
	};

	// show the list of sessions
	return (
		<div>
			{!showSession && (
				<div className={classes.root}>
					{(loading || saving) && <Loader />}
					{!loading && (
						<div className={classes.form}>
							<SessionTable />
							<Fab
								size="small"
								color="primary"
								aria-label="Add"
								className={classes.add}
								disabled={saving}
								onClick={() => {
									const newSession = JSON.parse(JSON.stringify(sessions[0]));
									newSession.id = generateSessionId();
									newSession.date = window.moment().format('Do MMMM YYYY');
									newSession.invoiceId = newSession.invoiceLink = newSession.receiptId = newSession.receiptLink =
										'';
									newSession.status = 'UNSAVED';
									setShowSession(newSession);
								}}
							>
								<AddIcon />
							</Fab>
						</div>
					)}
				</div>
			)}
			{showSession && (
				<SessionPage
					fields={showSession}
					onInvoice={onInvoice}
					onSave={onSave}
					onSendReceipt={onSendReceipt}
					onClose={() => {
						setShowSession(null);
						setHeading(name);
					}}
				/>
			)}
		</div>
	);
}

export default SessionList;
