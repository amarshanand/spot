import {
  sendEmail,
  generateInvoice,
  getSharableLink,
  downloadAsPDF,
  getFeedbackFormLink,
} from "../../api/sheets";

const sendNewInvoice = async (
  session,
  profile,
  openSpreadsheetFolderId,
  done
) => {
  const kvp = {};
  // obtain session and therapist details
  Object.entries(session).forEach(
    ([key, value]) => (kvp[`session_${key}`] = value)
  );
  Object.entries(profile).forEach(
    ([key, value]) => (kvp[`therapist_${key}`] = value)
  );
  // make this as the Invoice
  kvp[`doc_title`] = `INVOICE`;
  kvp[`footer_message`] = ``;
  // do some formatting and calulations
  kvp[`session_cost`] =
    kvp[`session_cost`] && !isNaN(kvp[`session_cost`])
      ? kvp[`session_cost`]
      : "0";
  kvp[`session_travel_cost`] =
    kvp[`session_travel_cost`] && !isNaN(kvp[`session_travel_cost`])
      ? kvp[`session_travel_cost`]
      : "0";
  kvp[`total_cost`] = `$ ${(
    parseFloat(kvp[`session_cost`]) + parseFloat(kvp[`session_travel_cost`])
  ).toFixed(2)}`;
  kvp[`session_cost`] = `$ ${parseFloat(kvp[`session_cost`]).toFixed(2)}`;
  kvp[`session_travel_cost`] = `$ ${parseFloat(
    kvp[`session_travel_cost`]
  ).toFixed(2)}`;
  kvp[`amount_paid`] = `$ 0.00`;
  kvp.invoice_timestamp = window.moment().format("Do MMMM YYYY");
  let { id, webViewLink, feedbackFormLink } = await generateInvoice(
    `${openSpreadsheetFolderId}/${session.name} Speech Pathology Invoice ${session.date}`,
    kvp
  );
  session.invoiceId = id;
  session.invoiceLink = webViewLink;
  session.feedbackFormLink = feedbackFormLink;
  await sendExistingInvoice(session, profile, openSpreadsheetFolderId, done);
  return session;
};

const sendExistingInvoice = async (session, profile, folderId, done) => {
  const pdfData = await downloadAsPDF(session.invoiceId);
  //openPDFinNewWindow(`${session.name}-${session.date}.pdf`, pdfData);
  // also, send a pdf view of the document in an email
  const kvp = {};
  Object.entries(session).forEach(
    ([key, value]) => (kvp[`session_${key}`] = value)
  );
  kvp[`session_cost`] =
    kvp[`session_cost`] && !isNaN(kvp[`session_cost`])
      ? kvp[`session_cost`]
      : "0";
  kvp[`session_travel_cost`] =
    kvp[`session_travel_cost`] && !isNaN(kvp[`session_travel_cost`])
      ? kvp[`session_travel_cost`]
      : "0";
  const totalCost = (
    parseFloat(kvp[`session_cost`]) + parseFloat(kvp[`session_travel_cost`])
  ).toFixed(2);
  sendEmail(
    session.email,
    `Speech Pathology Invoice for ${session.name} on ${session.date}`,
    `<p>Dear ${session.name},</p>` +
      `<p>Please find your invoice for <b>${session.date}</b> of value <b>$${totalCost}</b>. <br/></p>` +
      `<p><a href="${await getFeedbackFormLink(folderId, kvp)}">` +
      `Please click here for disputing this invoice or any other feedback</a>. <br/></p>` +
      `<p><a href="${await getSharableLink(
        folderId
      )}">Click here to view all of your invoices and receipts</a>. <br/></p>` +
      `<p>Regards,<br/>${profile.name}</p>`,
    pdfData,
    `${session.name} Speech Pathology Invoice ${session.date}`,
    done
  );
};

const sendNewReceipt = async (
  session,
  profile,
  openSpreadsheetFolderId,
  done
) => {
  const kvp = {};
  // obtain session and therapist details
  Object.entries(session).forEach(
    ([key, value]) => (kvp[`session_${key}`] = value)
  );
  Object.entries(profile).forEach(
    ([key, value]) => (kvp[`therapist_${key}`] = value)
  );
  // make this as the Receipt
  kvp[`doc_title`] = `RECEIPT`;
  kvp[`footer_message`] = `Thank you for clearing the invoice`;
  // do some formatting and calulations
  kvp[`session_cost`] =
    kvp[`session_cost`] && !isNaN(kvp[`session_cost`])
      ? kvp[`session_cost`]
      : "0";
  kvp[`session_travel_cost`] =
    kvp[`session_travel_cost`] && !isNaN(kvp[`session_travel_cost`])
      ? kvp[`session_travel_cost`]
      : "0";
  kvp[`amount_paid`] = `$ ${(
    parseFloat(kvp[`session_cost`]) + parseFloat(kvp[`session_travel_cost`])
  ).toFixed(2)}`;
  kvp[`session_cost`] = `$ ${parseFloat(kvp[`session_cost`]).toFixed(2)}`;
  kvp[`session_travel_cost`] = `$ ${parseFloat(
    kvp[`session_travel_cost`]
  ).toFixed(2)}`;
  kvp[`total_cost`] = `$ 0.00`;
  kvp.invoice_timestamp = window.moment().format("Do MMMM YYYY");
  let { id, webViewLink, feedbackFormLink } = await generateInvoice(
    `${openSpreadsheetFolderId}/Receipt : ${session.name} Speech Pathology Invoice ${session.date}`,
    kvp
  );
  session.receiptId = id;
  session.receiptLink = webViewLink;
  session.feedbackFormLink = feedbackFormLink;
  await sendExistingReceipt(session, profile, openSpreadsheetFolderId, done);
  return session;
};

const sendExistingReceipt = async (session, profile, folderId, done) => {
  const pdfData = await downloadAsPDF(session.receiptId);
  //openPDFinNewWindow(`${session.name}-${session.date}.pdf`, pdfData);
  // also, send a pdf view of the document in an email
  const kvp = {};
  Object.entries(session).forEach(
    ([key, value]) => (kvp[`session_${key}`] = value)
  );
  kvp[`session_cost`] =
    kvp[`session_cost`] && !isNaN(kvp[`session_cost`])
      ? kvp[`session_cost`]
      : "0";
  kvp[`session_travel_cost`] =
    kvp[`session_travel_cost`] && !isNaN(kvp[`session_travel_cost`])
      ? kvp[`session_travel_cost`]
      : "0";
  const totalCost = (
    parseFloat(kvp[`session_cost`]) + parseFloat(kvp[`session_travel_cost`])
  ).toFixed(2);
  sendEmail(
    session.email,
    `Receipt for Speech Pathology Invoice for ${session.name} on ${session.date}`,
    `<p>Dear ${session.name},</p>` +
      `<p>Thank you for settling the invoice for the therapy-session on <b>${session.date}</b> of value <b>$${totalCost}</b>. Please find the payment receipt enclosed.. <br/></p>` +
      `<p><a href="${await getSharableLink(
        folderId
      )}">Click here to view all of your invoices and receipts</a>. <br/></p>` +
      `<p>Regards,<br/>${profile.name}</p>`,
    pdfData,
    `Receipt: ${session.name} Speech Pathology Invoice ${session.date}`,
    done
  );
};

/**
 * open the given pdf data in a new window
 *
const openPDFinNewWindow = (name, pdfData) => {
	const winparams =
		'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,resizable,screenX=50,screenY=50,width=850,height=1050';
	const htmlText = `<embed width=100% height=100% type="application/pdf" src="data:application/pdf,${escape(
		pdfData
	)}"></embed>`;
	const detailWindow = window.open('', name, winparams);
	detailWindow.document.write(htmlText);
	detailWindow.document.close();
};
*/

export {
  sendNewInvoice,
  sendNewReceipt,
  sendExistingReceipt,
  sendExistingInvoice,
};
