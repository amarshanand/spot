import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Cancel';
import Icon from '@mdi/react';
import { mdiFilePdfBox } from '@mdi/js';
import Loader from '../../components/Loader';

import { useGlobalState } from '../../store';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		margin: '1rem auto'
	},
	form: {
		marginTop: '-1.75rem'
	},
	textField: {
		margin: '1rem'
	},
	save: {
		margin: theme.spacing.unit,
		position: 'fixed',
		bottom: '1rem',
		right: '1rem'
	},
	invoice: {
		margin: theme.spacing.unit,
		position: 'fixed',
		bottom: '1rem',
		left: '4.5rem'
	},
	close: {
		position: 'fixed',
		top: '4.5rem',
		right: '1rem',
		width: '2rem',
		height: '2rem',
		color: 'red'
	},
	table: {
		margin: '2rem 1rem'
	},
	row: {
		margin: '1rem 0',
		padding: '1rem 1rem 1.5rem 1rem',
		position: 'relative'
	},
	add: {
		width: '2rem',
		height: '2rem',
		minHeight: 'auto',
		marginLeft: '1rem',
		verticalAlign: 'bottom',
		boxShadow: 'none'
	},
	remove: {
		width: '1.25rem',
		height: '1.25rem',
		minHeight: 'auto',
		position: 'absolute',
		top: '0rem',
		right: '0.6rem',
		boxShadow: 'none'
	},
	pdf: {
		width: '1.5rem',
		height: '1.25rem',
		minHeight: 'auto',
		position: 'absolute',
		top: '0.8rem',
		right: '3.25rem',
		boxShadow: 'none',
		transform: 'rotate(-90deg)'
	}
}));

function Form({ loading, fields, styles, onSave, onInvoice, onClose }) {
	const classes = useStyles();
	const [ saving, setSaving ] = useState(false);
	const [ invoicing, setInvoicing ] = useState(false);
	const [ values, setValues ] = useState(fields);
	const [ , setNotification ] = useGlobalState('notification');
	const [ openSpreadsheetId ] = useGlobalState('openSpreadsheetId');
	const [ heading ] = useGlobalState('heading');

	useEffect(() => setValues(fields), [ fields, openSpreadsheetId ]);

	// update the state as the user types in. for first level elements, its simple. for second level, the name contains the full path
	const handleChange = (name) => (event) => setValues({ ...values, [name]: event.target.value });

	const doSave = async () => {
		try {
			setSaving(true);
			await onSave(values, sortedKeys);
			setSaving(false);
			setNotification({ text: `${heading} Saved`, type: 'success' });
		} catch (e) {
			setSaving(false);
			console.log(`Could not save : ${e}`);
			setNotification({ text: 'Could not save', type: 'error' });
		}
	};

	const doInvoice = async () => {
		try {
			setInvoicing(true);
			const recepient = await onInvoice(values);
			setInvoicing(false);
			setNotification({ text: `Invoice sent to ${recepient}`, type: 'success' });
		} catch (e) {
			setSaving(false);
			console.log(`Could not send Invoice : ${JSON.stringify(e)}`);
			setNotification({ text: 'Could not send Invoice', type: 'error' });
		}
	};

	// a single editable field with title and contents
	const getEditable = (key, value, style) => {
		// hidden things
		if (!style || style.hidden) return <span id={key} key={key} />;
		return (
			<TextField
				id={key}
				key={key}
				label={key.split('~')[0]}
				className={classes.textField}
				value={Boolean(value) ? value : ''}
				onChange={handleChange(key)}
				margin="normal"
				type={style.type || 'text'}
				disabled={style.readonly || Boolean(saving || invoicing)}
				style={{ ...style }}
			/>
		);
	};

	// starting from a list of nvps, look into the styles for each name, and return an array of keys sorted by 'order'
	const keysSortedByOrder = (nvps, _styles = styles) => {
		const keys = Object.entries(nvps)
			.sort((a, b) => {
				if (Array.isArray(a[1])) return a;
				if (Array.isArray(b[1])) return b;
				if (!_styles[a[0]] || !_styles[b[0]]) {
					console.log(`WARNING : Missing field from style : a=${a} and b=${b} and _styles=`);
					console.dir(_styles);
					debugger;
					return 0;
				}
				return _styles[a[0]].order - _styles[b[0]].order;
			})
			.map((e) => e[0])
			.filter((e) => _styles[e] && !_styles[e].hidden);
		return keys;
	};
	const sortedKeys = keysSortedByOrder(values);

	if (!styles) styles = {};
	return (
		<div className={classes.root}>
			{(loading || saving || invoicing) && <Loader />}
			{!loading && (
				<form className={classes.form} noValidate autoComplete="off">
					{keysSortedByOrder(values).map((key) => getEditable(key, values[key], styles[key]))}
					{onClose && <CloseIcon className={classes.close} onClick={onClose} />}
					{onInvoice && (
						<Fab
							size="small"
							color="primary"
							aria-label="Invoice"
							className={`${invoicing ? 'rotate' : ''} ${classes.invoice}`}
							disabled={Boolean(saving || invoicing)}
							onClick={doInvoice}
						>
							<Icon path={mdiFilePdfBox} color="white" size={1} />
						</Fab>
					)}
					<Fab
						size="small"
						color="primary"
						aria-label="Save"
						className={`${saving ? 'rotate' : ''} ${classes.save}`}
						disabled={Boolean(saving || invoicing)}
						onClick={doSave}
					>
						<SaveIcon />
					</Fab>
				</form>
			)}
		</div>
	);
}

export default Form;
