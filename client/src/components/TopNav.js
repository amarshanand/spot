import React from 'react';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@mdi/react';
import { mdiGoogleSpreadsheet, mdiGoogleDrive } from '@mdi/js';
import { useGlobalState } from '../store';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {},
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create([ 'width', 'margin' ], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create([ 'width', 'margin' ], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    menuButton: {
        marginLeft: '0, 12'
    },
    hide: {
        display: 'none'
    },
    toolbar: {
        justifyContent: 'space-between'
    },
    avatar: {
        marginRight: '1rem !important',
        width: '1.5rem',
        height: '1.5rem',
        display: 'inline-block',
        verticalAlign: 'bottom',
        border: '2px solid white',
        boxSizing: 'border-box'
    },
    icon: {
        verticalAlign: 'text-top'
    }
}));

function TopNav() {
    const classes = useStyles();
    const [ drawerOpen, setDrawerOpen ] = useGlobalState('drawerOpen');
    const [ profile ] = useGlobalState('profile');
    const [ heading ] = useGlobalState('heading');
    const [ openSpreadsheetId ] = useGlobalState('openSpreadsheetId');
    const [ spotdataFolderFolderId ] = useGlobalState('spotdataFolderFolderId');
    const [ currPage ] = useGlobalState('currPage');
    return (
        <AppBar
            position="fixed"
            className={classNames(classes.appBar, {
                [classes.appBarShift]: drawerOpen
            })}
        >
            <Toolbar disableGutters={!drawerOpen} className={classes.toolbar}>
                {profile && (
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={() => setDrawerOpen(true)}
                        className={classNames(classes.menuButton, {
                            [classes.hide]: drawerOpen
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                )}
                {/* title */}
                <Typography variant="h6" color="inherit" noWrap>
                    {heading}
                </Typography>
                {/* User Avatar */}
                {profile && (
                    <div>
                        {/* open in google drive */}
                        {currPage === 'LANDING_PAGE' && (
                            <IconButton
                                onClick={() =>
                                    window.open(
                                        `https://drive.google.com/drive/u/0/folders/${spotdataFolderFolderId}`,
                                        '_blank'
                                    )}
                            >
                                <Icon path={mdiGoogleDrive} color="white" size={1} className={classes.icon} />
                            </IconButton>
                        )}
                        {/* open in google sheets */}
                        {openSpreadsheetId && (
                            <IconButton
                                onClick={() =>
                                    window.open(
                                        `https://docs.google.com/spreadsheets/d/${openSpreadsheetId}`,
                                        '_blank'
                                    )}
                            >
                                <Icon path={mdiGoogleSpreadsheet} color="white" size={1} className={classes.icon} />
                            </IconButton>
                        )}
                        <IconButton disabled={true}>
                            {profile.imageUrl && (
                                <Avatar
                                    alt={profile.name}
                                    src={profile.imageUrl}
                                    className={classes.avatar}
                                    style={{ margin: 0 }}
                                />
                            )}
                        </IconButton>
                    </div>
                )}
            </Toolbar>
        </AppBar>
    );
}

export default TopNav;
