import React from 'react';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'absolute',
        width: '5rem',
        height: '5rem',
        top: 'calc(50% - 2.5rem)',
        left: 'calc(50% - 2.5rem + 1rem)',
        background: '#fafafa url(/img/loading.gif)',
        backgroundSize: 'contain',
        zIndex: 1
    }
}));

function Loader(props) {
    const classes = useStyles();
    return <div className={classes.root} />;
}

export default Loader;
