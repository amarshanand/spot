import React from 'react';
import classNames from 'classnames';
import { makeStyles, useTheme } from '@material-ui/styles';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import AddIcon from '@material-ui/icons/AddCircle';
import FaceIcon from '@material-ui/icons/Face';
import SettingsIcon from '@material-ui/icons/Settings';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import BugReportIcon from '@material-ui/icons/BugReport';
import Icon from '@mdi/react';
import { mdiGithubCircle } from '@mdi/js';
import { useGlobalState } from '../store';

import { settingsSpreadsheetId } from './../api/sheets';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex'
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: 'nowrap'
	},
	drawerOpen: {
		width: drawerWidth,
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	drawerClose: {
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		overflowX: 'hidden',
		width: theme.spacing.unit * 7 + 1,
		[theme.breakpoints.up('sm')]: {
			width: theme.spacing.unit * 8 - 2
		}
	},
	toolbar: {
		display: 'flex',
		alignItems: 'center',
		padding: '0 8px',
		...theme.mixins.toolbar
	},
	icon: {
		width: '1.5rem',
		height: '1.5rem'
	}
}));

function LeftDrawer() {
	const classes = useStyles();
	const theme = useTheme();
	const [ drawerOpen, setDrawerOpen ] = useGlobalState('drawerOpen');
	const [ currPage, setCurrPage ] = useGlobalState('currPage');
	const [ , setProfile ] = useGlobalState('profile');
	const [ , setHeading ] = useGlobalState('heading');
	const [ openSpreadsheetId, setOpenSpreadsheetId ] = useGlobalState('openSpreadsheetId');
	const [ , setOpenSpreadsheetFolderId ] = useGlobalState('openSpreadsheetFolderId');
	const [ patients ] = useGlobalState('patients');
	const [ settingsFound ] = useGlobalState('settingsFound');
	const [ debugs ] = useGlobalState('debugs');
	const [ showDebugs, setShowDebugs ] = useGlobalState('showDebugs');
	const [ , setShowSession ] = useGlobalState('showSession');

	function signOut() {
		window.gapi.auth2.getAuthInstance().signOut().then(() => {
			setDrawerOpen(false);
			setCurrPage('LOGIN_PAGE');
			setHeading('');
			setProfile(null);
		});
	}
	return (
		<Drawer
			variant="permanent"
			className={classNames(classes.drawer, {
				[classes.drawerOpen]: drawerOpen,
				[classes.drawerClose]: !drawerOpen
			})}
			classes={{
				paper: classNames({
					[classes.drawerOpen]: drawerOpen,
					[classes.drawerClose]: !drawerOpen
				})
			}}
			open={drawerOpen}
		>
			<div className={classes.toolbar}>
				<IconButton onClick={() => setDrawerOpen(false)}>
					{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
				</IconButton>
			</div>
			<Divider />
			{/* list of patients (one-click access) */}
			<List>
				<ListItem
					button
					key={'home'}
					onClick={() => {
						setHeading('Notices');
						setCurrPage('LANDING_PAGE');
					}}
					selected={currPage === 'LANDING_PAGE'}
				>
					<ListItemIcon>
						<HomeIcon />
					</ListItemIcon>
					<ListItemText primary={'Home Page'} />
				</ListItem>
			</List>
			<Divider />
			<List>
				{patients.map(({ folderId, recordsSpreadsheetId, name, color }) => {
					return (
						<ListItem
							button
							key={recordsSpreadsheetId}
							selected={openSpreadsheetId === recordsSpreadsheetId}
							onClick={() => {
								setHeading(name);
								setCurrPage('PATIENT_PAGE');
								setOpenSpreadsheetId(recordsSpreadsheetId);
								setOpenSpreadsheetFolderId(folderId);
								setShowSession(null);
							}}
						>
							<ListItemIcon>
								<FaceIcon nativeColor={color} />
							</ListItemIcon>
							<ListItemText primary={name} />
						</ListItem>
					);
				})}
				<ListItem
					button
					key={'add'}
					disabled={!settingsFound}
					selected={currPage === 'PATIENT_PAGE' && !openSpreadsheetId}
					onClick={() => {
						setHeading('New Patient');
						setCurrPage('PATIENT_PAGE');
						setOpenSpreadsheetId(null);
						setOpenSpreadsheetFolderId(null);
						setShowSession(null);
					}}
				>
					<ListItemIcon>
						<AddIcon />
					</ListItemIcon>
					<ListItemText primary={'Add Patient'} />
				</ListItem>
			</List>
			<Divider />
			<List>
				<ListItem
					button
					key={'settings'}
					selected={currPage === 'SETTINGS_PAGE'}
					onClick={() => {
						setOpenSpreadsheetId(settingsSpreadsheetId);
						setCurrPage('SETTINGS_PAGE');
						setShowSession(null);
					}}
				>
					<ListItemIcon>
						<SettingsIcon />
					</ListItemIcon>
					<ListItemText primary={'Settings'} />
				</ListItem>
				{/* logout from google */}
				<ListItem button key={'Logout'} onClick={signOut}>
					<ListItemIcon>
						<LogoutIcon />
					</ListItemIcon>
					<ListItemText primary={'Logout'} />
				</ListItem>
			</List>
			<Divider />
			<List>
				{/* debugger */}
				{debugs &&
				debugs.trim().length > 0 && (
					<ListItem button key={'Debugger'} onClick={() => setShowDebugs(!showDebugs)}>
						<ListItemIcon>
							<BugReportIcon />
						</ListItemIcon>
						<ListItemText primary={'Debugger'} />
					</ListItem>
				)}
				{/* gitlab */}
				<ListItem
					button
					key={'Gitlab'}
					onClick={() => window.open(`https://gitlab.com/amarsh.hk/spot`, '_blank')}
				>
					<ListItemIcon>
						<Icon path={mdiGithubCircle} color="#757575" size={1} className={classes.icon} />
					</ListItemIcon>
					<ListItemText primary={'Gitlab'} />
				</ListItem>
			</List>
		</Drawer>
	);
}

export default LeftDrawer;
