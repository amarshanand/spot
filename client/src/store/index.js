import { createGlobalState } from 'react-hooks-global-state';

const initialState = {
	drawerOpen: false,
	currPage: 'LOGIN_PAGE',
	events: [],
	heading: '',
	profile: null,
	settingsFound: null,
	loading: null,
	spotdataFolderFolderId: null,
	openSpreadsheetId: null,
	openSpreadsheetFolderId: null,
	patients: [],
	notification: null,
	debugs: '',
	showDebugs: false,
	showSession: null,
	confirm: { show: false, text: '', ok: () => {} }
};

export const { GlobalStateProvider, useGlobalState } = createGlobalState(initialState);
