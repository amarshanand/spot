import React from "react";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";

import theme from "./theme";
import "./App.css";

import TopNav from "./components/TopNav";
import LeftDrawer from "./components/LeftDrawer";
import Notification from "./components/Notification";
import Confirm from "./components/Confirm";
import LoginPage from "./pages/LoginPage";
import LandingPage from "./pages/LandingPage";
import PatientPage from "./pages/PatientPage";
import SettingsPage from "./pages/SettingsPage";

import { useGlobalState } from "./store/";

const styles = {
  root: {
    display: "flex",
  },
  home: {
    display: "block",
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    padding: "0 8px",
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    overflowX: "hidden",
  },
  debugs: {
    position: "fixed",
    left: "3.5rem",
    bottom: 0,
    width: "calc(100% - 3.5rem)",
    height: "40rem",
    maxHeight: "40rem",
    overflowY: "scroll",
    background: "rgb(37,37,37)",
    color: "lime",
    padding: "1rem",
  },
  build: {
    position: "fixed",
    bottom: "0.5rem",
    left: "0.5rem",
    fontSize: "0.5rem",
    width: "0.1rem",
    wordBreak: "break-all",
    color: "rgb(225, 0, 80)",
    zIndex: 111111111,
  },
};

const App = (props) => {
  const { classes } = props;
  const [currPage] = useGlobalState("currPage");
  const [debugs] = useGlobalState("debugs");
  const [showDebugs] = useGlobalState("showDebugs");
  return (
    <MuiThemeProvider theme={theme}>
      <div className={currPage === "LOGIN_PAGE" ? classes.home : classes.root}>
        <CssBaseline />
        {currPage === "LOGIN_PAGE" && <LoginPage />}
        {currPage !== "LOGIN_PAGE" && <TopNav />}
        {currPage !== "LOGIN_PAGE" && <LeftDrawer />}
        <main className={classes.content}>
          {currPage !== "LOGIN_PAGE" && <div className={classes.toolbar} />}
          {currPage === "LANDING_PAGE" && <LandingPage />}
          {currPage === "PATIENT_PAGE" && <PatientPage />}
          {currPage === "SETTINGS_PAGE" && <SettingsPage />}
        </main>
        <Notification />
        <Confirm />
        {showDebugs && (
          <div className={classes.debugs}>
            <code>
              <pre>{debugs}</pre>
            </code>
          </div>
        )}
      </div>
      <div className={classes.build}>8</div>
    </MuiThemeProvider>
  );
};

export default withStyles(styles)(App);
