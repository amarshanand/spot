## Account Registration
We would have two types of roles:
- Therapist
- Patient
Therapist will create the account, and in the process, will invite patients. Each patient will receive an invitation to join. A patient cannot join on their own.
Lets say there is one workbook per patient. Hence, for Account registration, there would be one sheet, which would contain details of the patient, including whether the patient has joined or not.

### Landing page for a Therapist
- start with Google login
- when logged-in, connect to Google Sheets
- CRUD patient
- Create patient:
    - A Form for asking patient details
    - An email sent to the patient for approval (patient can change a few fields)
    - An acknowledgement from the patient
    - An email notification to the Therapist